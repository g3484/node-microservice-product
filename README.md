# GoodFood - NodeJS => Product

## Description

GoodFood est une solution de livraison de plats cuisinés.

Cette solution comporte un site web ainsi qu'une application mobile, et est composée de plusieurs micro services effectuant des actions spécifiques.

Dans ce repository, le microservice NodeJS gère les catégories de produit et les produits de la solution GoodFood.

### Fonctionnalités

Le microservice NodeJS inclus les fonctionnalités suivantes :

- **Lister les catégories de produit** : /api/products/category
- **Lister les produits** : /api/products
- **Lister les produits par catégorie de produit** : /api/products/categoryId/:idCategory
- **Voir le détail d'un produit selon son identifiant** : /api/products/:id
