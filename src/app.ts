import express from 'express';
import 'express-async-errors';
import { currentUser, errorHandler, NotFoundError } from '@goodfood/common';
import { json } from 'body-parser';

import { ListCategoryProductRouter } from './routes/category';
import { CategoryProductByIdRouter } from './routes/category/show';
import { DeleteCategoryProductRouter } from './routes/category/delete';
import { CreateCategoryProductRouter } from './routes/category/new';
import { UpdateCategoryProductRouter } from './routes/category/update';
import { ResetCategoryProductRouter } from './routes/category/resetDb';

import { ListProductRouter } from './routes/product';
import { ProductByIdRouter } from './routes/product/show';
import { ProductByCatIdRouter } from './routes/product/getByCat';
import { CreateProductRouter } from './routes/product/new';
import { DeleteProductRouter } from './routes/product/delete';
import { UpdateProductRouter } from './routes/product/update';
import { ResetProductRouter } from './routes/product/resetDb';

const app = express();

app.set('trust proxy', true);
app.use(json());

app.use(currentUser);

app.all('/api/products/health', (req, res) => {
  res.send('Hello world');
});

app.use(CreateProductRouter);
app.use(ListProductRouter);
app.use(UpdateProductRouter);
app.use(DeleteProductRouter);
app.use(ProductByCatIdRouter);
app.use(ResetProductRouter);

app.use(CreateCategoryProductRouter);
app.use(ListCategoryProductRouter);
app.use(ResetCategoryProductRouter);
app.use(DeleteCategoryProductRouter);
app.use(UpdateCategoryProductRouter);

app.use(CategoryProductByIdRouter);
app.use(ProductByIdRouter);

app.all('*', async (req, res) => {
  throw new NotFoundError();
});

app.use(errorHandler);

export { app };
