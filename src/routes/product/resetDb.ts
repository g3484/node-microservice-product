import express, { Request, Response } from 'express';
import { ProductCreatedPublisher } from '../../events/publishers/product-created-publisher';
import { Product } from '../../models/Product';
import { natsWrapper } from '../../nats-wrapper';

const router = express.Router();

router.post('/api/products/reset', async (req: Request, res: Response) => {
  // get data from json file in data folder
  const data = require('../../data/products.json');

  let hasError = false;
  for (const product of data) {
    const {
      name,
      _id,
      description,
      price,
      idCategory,
      imageUrl,
      publicIdImage,
    } = product;

    const id = _id['$oid'];
    try {
      const product = Product.build({
        _id: id,
        name,
        description,
        price,
        idCategory,
        imageUrl,
        publicIdImage,
      });

      await product.save();

      await new ProductCreatedPublisher(natsWrapper.client).publish({
        id: product.id,
        version: product.version,
        name: product.name,
        description: product.description,
        price: product.price,
        idCategory: product.idCategory,
        imageUrl: product.imageUrl,
        publicIdImage: product.publicIdImage,
      });
    } catch (err) {
      console.log(err);
      hasError = true;
    }
  }

  if (hasError) {
    res.status(400).send({ message: 'Error while loading data' });
  } else {
    res.status(201).send({ message: 'Products reset' });
  }
});

export { router as ResetProductRouter };
