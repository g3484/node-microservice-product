import express, { Request, Response } from 'express';
import fs from 'fs';
import { body } from 'express-validator';
import {
  requireAuth,
  validateRequest,
  NotFoundError,
  ServerError,
  BadRequestError,
} from '@goodfood/common';
import { Product } from '../../models/Product';
import { cloudinary } from '../../config/cloudinary';
import { ProductUpdatedPublisher } from '../../events/publishers/product-updated-publisher';
import { natsWrapper } from '../../nats-wrapper';
import { upload } from '../../config/multer';

const router = express.Router();

router.put(
  '/api/products/:id',
  requireAuth,
  upload.single('image'),
  [
    body('name').notEmpty().withMessage('name must be provided'),
    body('description').notEmpty().withMessage('description must be provided'),
    body('price').isNumeric().notEmpty().withMessage('price must be provided'),
    body('idCategory').notEmpty().withMessage('idCategory must be provided'),
  ],
  validateRequest,
  async (req: Request, res: Response) => {
    const { name, description, price, idCategory } = req.body;
    const file = req.file;

    if (file) {
      const { path, filename } = file;

      const product = await Product.findById(req.params.id);

      if (!product) {
        throw new NotFoundError();
      }

      const publicId = product.publicIdImage;

      try {
        await cloudinary.uploader.destroy(publicId);

        const response = await cloudinary.uploader.upload(path, {
          public_id: filename,
        });

        const { url, public_id } = response;

        fs.unlinkSync(path);

        product.set({
          name: name,
          description: description,
          price: price,
          idCategory: idCategory,
          imageUrl: url,
          publicIdImage: public_id,
        });

        await product.save();

        // Publish an event saying that a product was updated
        await new ProductUpdatedPublisher(natsWrapper.client).publish({
          id: product.id,
          version: product.version,
          name: product.name,
          description: product.description,
          price: product.price,
          idCategory: product.idCategory,
          imageUrl: product.imageUrl,
          publicIdImage: product.publicIdImage,
        });

        res.status(200).send(product);
      } catch (err) {
        throw new ServerError();
      }
    } else {
      const product = await Product.findById(req.params.id);

      if (!product) {
        throw new NotFoundError();
      }

      product.set({
        name: name,
        description: description,
        price: price,
        idCategory: idCategory,
      });

      await product.save();

      // Publish an event saying that a product was updated
      await new ProductUpdatedPublisher(natsWrapper.client).publish({
        id: product.id,
        version: product.version,
        name: product.name,
        description: product.description,
        price: product.price,
        idCategory: product.idCategory,
        imageUrl: product.imageUrl,
        publicIdImage: product.publicIdImage,
      });

      res.status(200).send(product);
    }
  }
);

export { router as UpdateProductRouter };
