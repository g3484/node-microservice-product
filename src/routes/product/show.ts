import express from 'express';
import { NotFoundError } from '@goodfood/common';
import { Product } from '../../models/Product';

const router = express.Router();

//Recherche d'un produit par son id
router.get('/api/products/:id', async (req, res) => {
  const product = await Product.findById(req.params.id);

  if (!product) {
    throw new NotFoundError();
  }

  res.status(200).send(product);
});

export { router as ProductByIdRouter };
