import express, { Request, Response } from 'express';
import fs from 'fs';
import { body } from 'express-validator';
import {
  validateRequest,
  requireAuth,
  BadRequestError,
} from '@goodfood/common';
import { Product } from '../../models/Product';
import { cloudinary } from '../../config/cloudinary';
import { ProductCreatedPublisher } from '../../events/publishers/product-created-publisher';
import { natsWrapper } from '../../nats-wrapper';
import { upload } from '../../config/multer';

const router = express.Router();

router.post(
  '/api/products',
  requireAuth,
  upload.single('image'),
  [
    body('name').not().isEmpty().withMessage('name must be provided'),
    body('description')
      .not()
      .isEmpty()
      .withMessage('description must be provided'),
    body('price')
      .isFloat({ gt: 0 })
      .withMessage('Price must be greater than 0'),
    body('idCategory')
      .not()
      .isEmpty()
      .withMessage('idCategory must be provided'),
  ],
  validateRequest,
  async (req: Request, res: Response) => {
    const { name, description, price, idCategory } = req.body;
    const file = req.file;

    if (!file) {
      throw new BadRequestError(
        'Please upload a file with .png, .jpg, .jpeg format'
      );
    }

    const { path, filename } = file;

    const response = await cloudinary.uploader.upload(path, {
      public_id: filename,
    });

    const { url, public_id } = response;

    fs.unlinkSync(path);

    const product = Product.build({
      name: name,
      description: description,
      price: price,
      idCategory: idCategory,
      imageUrl: url,
      publicIdImage: public_id,
    });

    await product.save();

    await new ProductCreatedPublisher(natsWrapper.client).publish({
      id: product.id,
      version: product.version,
      name: product.name,
      description: product.description,
      price: product.price,
      idCategory: product.idCategory,
      imageUrl: product.imageUrl,
      publicIdImage: product.publicIdImage,
    });

    res.status(201).send(product);
  }
);

export { router as CreateProductRouter };
