import express from 'express';
import { NotFoundError } from '@goodfood/common';
import { Product } from '../../models/Product';

const router = express.Router();

//Recherche d'un produit par son id
router.get('/api/products/categoryId/:idCategory', async (req, res) => {
  const productsByCat = await Product.find({
    idCategory: req.params.idCategory,
  });

  if (!productsByCat) {
    throw new NotFoundError();
  }

  res.status(200).send(productsByCat);
});

export { router as ProductByCatIdRouter };
