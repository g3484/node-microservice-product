import express, { Request, Response } from 'express';
import { body } from 'express-validator';
import { Product } from '../../models/Product';
import {
  requireAuth,
  validateRequest,
  BadRequestError,
  NotFoundError,
} from '@goodfood/common';
import { cloudinary } from '../../config/cloudinary';

const router = express.Router();

//Suppression d'un produit dans la BDD + suppression image dans cloudinary selon sa public id
router.delete(
  '/api/products/:id',
  requireAuth,
  async (req: Request, res: Response) => {
    const product = await Product.findById(req.params.id);

    if (!product) {
      throw new NotFoundError();
    }

    try {
      const publicId = product.publicIdImage;

      await cloudinary.uploader.destroy(publicId);

      await product.delete();
      res.status(200).send({ message: 'Produit supprimé !' });
    } catch (err) {
      throw new BadRequestError('Product not found');
    }
  }
);

export { router as DeleteProductRouter };
