import express from 'express';
import { Product } from '../../models/Product';

const router = express.Router();

//Liste de tous les produits
router.get('/api/products', async (req, res) => {
  const products = await Product.find({});
  return res.status(200).send(products);
});

export { router as ListProductRouter };
