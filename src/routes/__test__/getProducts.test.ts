import request from 'supertest';
import { app } from '../../app';

it('has a route handler listening to /api/products/ for GET request', async () => {
  const response = await request(app).get('/api/products').send();
  expect(response.status).not.toEqual(404);
});
