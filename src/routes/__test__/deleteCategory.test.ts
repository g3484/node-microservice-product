import request from 'supertest';
import { app } from '../../app';
import mongoose from 'mongoose';

it('returns a 404 if the category id does not exist', async () => {
  const id = new mongoose.Types.ObjectId().toHexString();
  await request(app)
    .delete(`/api/products/category/${id}`)
    .set('Authorization', `bearer ${global.signin()}`)
    .expect(404);
});

it('can only be accessed if the user is signed in', async () => {
  const id = new mongoose.Types.ObjectId().toHexString();
  await request(app)
    .delete(`/api/products/category/${id}`)
    .send({})
    .expect(401);
});

it('returns a status other than 401 if the user is signed in', async () => {
  const id = new mongoose.Types.ObjectId().toHexString();
  const response = await request(app)
    .delete(`/api/products/categoryy/${id}`)
    .set('Authorization', `bearer ${global.signin()}`);
  expect(response.status).not.toEqual(401);
});

it('delete the category with valid id', async () => {
  const name = 'test';

  const response = await request(app)
    .post('/api/products/category')
    .set('Authorization', `bearer ${global.signin()}`)
    .send({
      name: name,
    })
    .expect(201);

  await request(app)
    .delete(`/api/products/category/${response.body.id}`)
    .set('Authorization', `bearer ${global.signin()}`)
    .expect(200);
});
