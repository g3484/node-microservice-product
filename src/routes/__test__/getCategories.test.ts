import request from 'supertest';
import { app } from '../../app';

const createCategory = () => {
  return request(app)
    .post('/api/products/category')
    .set('Authorization', `bearer ${global.signin()}`)
    .send({
      name: 'Une catégorie',
    });
};

it('can fetch a list of categories', async () => {
  await createCategory();
  await createCategory();
  await createCategory();
  await createCategory();

  const response = await request(app)
    .get('/api/products/category')
    .send()
    .expect(200);

  expect(response.body.length).toEqual(4);
});
