import request from 'supertest';
import { app } from '../../app';

it('has a route handler listening to /api/products for POST request', async () => {
  const response = await request(app).post('/api/products').send({});
  expect(response.status).not.toEqual(404);
});

it('can only be accessed if the user is signed in', async () => {
  await request(app).post('/api/products').send({}).expect(401);
});

it('returns a status other than 401 if the user is signed in', async () => {
  const response = await request(app)
    .post('/api/products')
    .set('Authorization', `bearer ${global.signin()}`)
    .send({});

  expect(response.status).not.toEqual(401);
});

it('returns an error if an invalid name is provided', async () => {
  const image = {
    fieldname: 'image',
    originalname: 'test.jpg',
    encoding: '7bit',
    mimetype: 'image/jpeg',
    buffer: Buffer.from('test'),
    size: 123,
  };

  await request(app)
    .post('/api/products')
    .set('Authorization', `bearer ${global.signin()}`)
    .send({
      name: '',
      description: 'Test',
      price: 10,
      idCategory: '3',
      image: image,
    })
    .expect(400);

  await request(app)
    .post('/api/products')
    .set('Authorization', `bearer ${global.signin()}`)
    .send({
      description: 'Test',
      price: 10,
      idCategory: '3',
      image: image,
    })
    .expect(400);
});

it('returns an error if an invalid description is provided', async () => {
  await request(app)
    .post('/api/products')
    .set('Authorization', `bearer ${global.signin()}`)
    .send({
      name: 'test',
      description: '',
      price: 10,
      idCategory: '3',
      imageUrl:
        'https://i.picsum.photos/id/10/2500/1667.jpg?hmac=J04WWC_ebchx3WwzbM-Z4_KC_LeLBWr5LZMaAkWkF68',
    })
    .expect(400);

  await request(app)
    .post('/api/products')
    .set('Authorization', `bearer ${global.signin()}`)
    .send({
      title: 'test',
      price: 10,
      idCategory: '3',
      imageUrl:
        'https://i.picsum.photos/id/10/2500/1667.jpg?hmac=J04WWC_ebchx3WwzbM-Z4_KC_LeLBWr5LZMaAkWkF68',
    })
    .expect(400);
});

it('returns an error if an invalid price is provided', async () => {
  await request(app)
    .post('/api/products')
    .set('Authorization', `bearer ${global.signin()}`)
    .send({
      name: 'test',
      description: 'test',
      price: -10,
      idCategory: '3',
      imageUrl:
        'https://i.picsum.photos/id/10/2500/1667.jpg?hmac=J04WWC_ebchx3WwzbM-Z4_KC_LeLBWr5LZMaAkWkF68',
    })
    .expect(400);

  await request(app)
    .post('/api/products')
    .set('Authorization', `bearer ${global.signin()}`)
    .send({
      title: 'test',
      description: 'test',
      idCategory: '3',
      imageUrl:
        'https://i.picsum.photos/id/10/2500/1667.jpg?hmac=J04WWC_ebchx3WwzbM-Z4_KC_LeLBWr5LZMaAkWkF68',
    })
    .expect(400);
});

it('returns an error if an invalid category is provided', async () => {
  await request(app)
    .post('/api/products')
    .set('Authorization', `bearer ${global.signin()}`)
    .send({
      name: 'test',
      description: 'test',
      price: 10,
      idCategory: '',
      imageUrl:
        'https://i.picsum.photos/id/10/2500/1667.jpg?hmac=J04WWC_ebchx3WwzbM-Z4_KC_LeLBWr5LZMaAkWkF68',
    })
    .expect(400);

  await request(app)
    .post('/api/products')
    .set('Authorization', `bearer ${global.signin()}`)
    .send({
      title: 'test',
      description: 'test',
      price: 10,
      imageUrl:
        'https://i.picsum.photos/id/10/2500/1667.jpg?hmac=J04WWC_ebchx3WwzbM-Z4_KC_LeLBWr5LZMaAkWkF68',
    })
    .expect(400);
});

it('returns an error if an invalid image is provided', async () => {
  await request(app)
    .post('/api/products')
    .set('Authorization', `bearer ${global.signin()}`)
    .send({
      name: 'test',
      description: 'test',
      price: 10,
      idCategory: '3',
      image: '',
    })
    .expect(400);

  await request(app)
    .post('/api/products')
    .set('Authorization', `bearer ${global.signin()}`)
    .send({
      title: 'test',
      description: 'test',
      price: 10,
      idCategory: '3',
    })
    .expect(400);
});
