import request from 'supertest';
import { app } from '../../app';
import mongoose from 'mongoose';

it('returns a 401 if the admin is not authenticated', async () => {
  const id = new mongoose.Types.ObjectId().toHexString();
  await request(app)
    .put(`/api/products/${id}`)
    .send({
      name: 'Test',
      description: 'Test',
      price: '24.50',
      idCategory: '3',
      imageUrl:
        'https://i.picsum.photos/id/10/2500/1667.jpg?hmac=J04WWC_ebchx3WwzbM-Z4_KC_LeLBWr5LZMaAkWkF68',
    })
    .expect(401);
});

it('returns a 400 if the admin provide invalid inputs', async () => {
  const id = new mongoose.Types.ObjectId().toHexString();
  await request(app)
    .put(`/api/products/${id}`)
    .set('Authorization', `bearer ${global.signin()}`)
    .send({
      name: '',
      description: 'Test',
      price: 10,
      idCategory: '3',
      imageUrl:
        'https://i.picsum.photos/id/10/2500/1667.jpg?hmac=J04WWC_ebchx3WwzbM-Z4_KC_LeLBWr5LZMaAkWkF68',
    })
    .expect(400);

  await request(app)
    .put(`/api/products/${id}`)
    .set('Authorization', `bearer ${global.signin()}`)
    .send({
      description: 'Test',
      price: 10,
      idCategory: '3',
      imageUrl:
        'https://i.picsum.photos/id/10/2500/1667.jpg?hmac=J04WWC_ebchx3WwzbM-Z4_KC_LeLBWr5LZMaAkWkF68',
    })
    .expect(400);
});
