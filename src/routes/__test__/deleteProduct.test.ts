import request from 'supertest';
import { app } from '../../app';
import mongoose from 'mongoose';

it('returns a 404 if the product id does not exist', async () => {
  const id = new mongoose.Types.ObjectId().toHexString();
  await request(app)
    .delete(`/api/products/${id}`)
    .set('Authorization', `bearer ${global.signin()}`)
    .expect(404);
});

it('can only be accessed if the user is signed in', async () => {
  const id = new mongoose.Types.ObjectId().toHexString();
  await request(app).delete(`/api/products/${id}`).send({}).expect(401);
});

it('returns a status other than 401 if the user is signed in', async () => {
  const id = new mongoose.Types.ObjectId().toHexString();
  const response = await request(app)
    .delete(`/api/products/${id}`)
    .set('Authorization', `bearer ${global.signin()}`);
  expect(response.status).not.toEqual(401);
});
