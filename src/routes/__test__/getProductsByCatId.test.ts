import request from 'supertest';
import { app } from '../../app';
import mongoose from 'mongoose';

it('returns a 404 if the category id does not exist', async () => {
  const id = new mongoose.Types.ObjectId().toHexString();
  await request(app)
    .get(`/api/products/${id}`)
    .set('Authorization', `bearer ${global.signin()}`)
    .expect(404);
});
