import request from 'supertest';
import { app } from '../../app';
import { Category } from '../../models/category';

it('has a route handler listening to /api/products/category for POST request', async () => {
  const response = await request(app).post('/api/products/category').send({});
  expect(response.status).not.toEqual(404);
});

it('can only be accessed if the user is signed in', async () => {
  await request(app).post('/api/products/category').send({}).expect(401);
});

it('returns a status other than 401 if the user is signed in', async () => {
  const response = await request(app)
    .post('/api/products/category')
    .set('Authorization', `bearer ${global.signin()}`)
    .send({});

  expect(response.status).not.toEqual(401);
});

it('returns an error if an invalid name is provided', async () => {
  await request(app)
    .post('/api/products/category')
    .set('Authorization', `bearer ${global.signin()}`)
    .send({
      name: '',
    })
    .expect(400);

  await request(app)
    .post('/api/products/category')
    .set('Authorization', `bearer ${global.signin()}`)
    .send({})
    .expect(400);
});

it('create a category with valid inputs', async () => {
  let categories = await Category.find({});
  expect(categories.length).toEqual(0);

  await request(app)
    .post('/api/products/category')
    .set('Authorization', `bearer ${global.signin()}`)
    .send({
      name: 'Test',
    })
    .expect(201);

  categories = await Category.find({});
  expect(categories.length).toEqual(1);
  expect(categories[0].name).toEqual('Test');
});
