import request from 'supertest';
import { app } from '../../app';
import mongoose from 'mongoose';

it('returns a 404 if the category is not found', async () => {
  const id = new mongoose.Types.ObjectId().toHexString();
  await request(app).get(`/api/products/category/${id}`).send().expect(404);
});

it('returns the category if the category is found', async () => {
  const name = 'test';
  const response = await request(app)
    .post('/api/products/category')
    .set('Authorization', `bearer ${global.signin()}`)
    .send({
      name: name,
    })
    .expect(201);

  const categoryResponse = await request(app)
    .get(`/api/products/category/${response.body.id}`)
    .send()
    .expect(200);

  expect(categoryResponse.body.name).toEqual(name);
});
