import request from 'supertest';
import { app } from '../../app';
import mongoose from 'mongoose';

it('returns a 404 if the category id does not exist', async () => {
  const id = new mongoose.Types.ObjectId().toHexString();
  await request(app)
    .put(`/api/products/category/${id}`)
    .set('Authorization', `bearer ${global.signin()}`)
    .send({
      name: 'Une catégorie',
    })
    .expect(404);
});

it('returns a 401 if the admin is not authenticated', async () => {
  const id = new mongoose.Types.ObjectId().toHexString();
  await request(app)
    .put(`/api/products/category/${id}`)
    .send({
      name: 'Une catégorie',
    })
    .expect(401);
});

it('returns a 400 if the admin provide invalid name', async () => {
  const id = new mongoose.Types.ObjectId().toHexString();
  await request(app)
    .put(`/api/products/category/${id}`)
    .set('Authorization', `bearer ${global.signin()}`)
    .send({
      name: '',
    })
    .expect(400);

  await request(app)
    .put(`/api/products/category/${id}`)
    .set('Authorization', `bearer ${global.signin()}`)
    .send({})
    .expect(400);
});

it('updates the category provided valid inputs', async () => {
  const name = 'test';
  const response = await request(app)
    .post('/api/products/category')
    .set('Authorization', `bearer ${global.signin()}`)
    .send({
      name: name,
    })
    .expect(201);

  const productResponse = await request(app)
    .put(`/api/products/category/${response.body.id}`)
    .set('Authorization', `bearer ${global.signin()}`)
    .send({
      name: 'name',
    })
    .expect(200);

  expect(productResponse.body.name).not.toEqual(name);
});
