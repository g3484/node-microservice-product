import express, { Request, Response } from 'express';
import { body } from 'express-validator';
import { requireAuth, validateRequest, ServerError } from '@goodfood/common';
import { Category } from '../../models/category';

const router = express.Router();

router.post(
  '/api/products/category',
  requireAuth,
  [body('name').notEmpty().withMessage('name must be provided')],
  validateRequest,
  async (req: Request, res: Response) => {
    const { name } = req.body;

    try {
      const category = Category.build({
        name: name,
      });

      await category.save();
      res.status(201).send(category);
    } catch (err) {
      throw new ServerError();
    }
  }
);

export { router as CreateCategoryProductRouter };
