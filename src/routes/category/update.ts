import express, { Request, Response } from 'express';
import { body } from 'express-validator';
import { requireAuth, validateRequest, NotFoundError } from '@goodfood/common';
import { Category } from '../../models/category';

const router = express.Router();

router.put(
  '/api/products/category/:id',
  requireAuth,
  [body('name').isString().notEmpty().withMessage('name must be provided')],
  validateRequest,
  async (req: Request, res: Response) => {
    const { name } = req.body;

    const category = await Category.findById(req.params.id);

    if (!category) {
      throw new NotFoundError();
    }

    category.set({
      name: name,
    });

    await category.save();
    res.status(200).send(category);
  }
);

export { router as UpdateCategoryProductRouter };
