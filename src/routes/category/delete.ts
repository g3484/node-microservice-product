import express, { Request, Response } from 'express';
import { body } from 'express-validator';
import { Category } from '../../models/category';
import { requireAuth, validateRequest, NotFoundError } from '@goodfood/common';

const router = express.Router();

router.delete(
  '/api/products/category/:id',
  requireAuth,
  async (req: Request, res: Response) => {
    const category = await Category.findById(req.params.id);

    if (!category) {
      throw new NotFoundError();
    }

    await category.delete();
    res.status(200).send({ message: 'Catégorie de produit supprimée !' });
  }
);

export { router as DeleteCategoryProductRouter };
