import express from 'express';
import { BadRequestError, NotFoundError } from '@goodfood/common';
import { Category } from '../../models/category';

const router = express.Router();

router.get('/api/products/category/:id', async (req, res) => {
  const category = await Category.findById(req.params.id);

  if (!category) {
    throw new NotFoundError();
  }

  res.status(200).send(category);
});

export { router as CategoryProductByIdRouter };
