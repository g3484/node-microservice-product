import express from 'express';
import { Category } from '../../models/category';
import { ServerError } from '@goodfood/common';
const router = express.Router();

//Liste de tous les catégories de produit
router.get('/api/products/category', async (req, res) => {
  const categories = await Category.find({});
  return res.status(200).send(categories);
});

export { router as ListCategoryProductRouter };
