import express, { Request, Response } from 'express';
import { body } from 'express-validator';
import { requireAuth, validateRequest, ServerError } from '@goodfood/common';
import { Category } from '../../models/category';

const router = express.Router();

router.post(
  '/api/products/categoryReset',
  async (req: Request, res: Response) => {
    // get data from json file in data folder
    const data = require('../../data/categoryproducts.json');

    let hasError = false;
    for (const product of data) {
      const { name, _id } = product;

      const id = _id['$oid'];
      try {
        const category = Category.build({
          _id: id,
          name,
        });
        await category.save();
      } catch (err) {
        console.log(err);
        hasError = true;
      }
    }

    if (hasError) {
      res.status(400).send({ message: 'Error while loading data' });
    } else {
      res.status(201).send({ message: 'Category products reset' });
    }
  }
);

export { router as ResetCategoryProductRouter };
