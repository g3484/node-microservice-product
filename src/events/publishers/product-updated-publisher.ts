import { Publisher, Subjects, ProductUpdatedEvent } from '@goodfood/common';

export class ProductUpdatedPublisher extends Publisher<ProductUpdatedEvent> {
  readonly subject = Subjects.ProductUpdated;
}
