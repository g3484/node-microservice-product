import { Publisher, Subjects, ProductCreatedEvent } from '@goodfood/common';

export class ProductCreatedPublisher extends Publisher<ProductCreatedEvent> {
  readonly subject = Subjects.ProductCreated;
}
